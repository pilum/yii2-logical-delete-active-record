CREATE TABLE test
(
  id serial NOT NULL,
  /* Тут можно использовать bool (и, чаще всего, так будет лучше), но, поскольку я не добавлял поле updated,
  то решил записывать временную метку прямо в то же поле. Индекс не добавлял намеренно. */
  deleted timestamp with time zone,
  CONSTRAINT tab1_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE tab1  IS 'Тестовая таблица';
COMMENT ON COLUMN tab1.id  IS 'Идентификатор строки';
COMMENT ON COLUMN tab1.deleted  IS 'Признак удаления строки, содержит NULL или временную метку удаления';
