<?php

namespace app\exceptions;

use Exception;

class RecordAlreadyDeletedException extends Exception
{
    /**
     * @inheritdoc
     */
    public function __construct($message="Can't delete this record doubly", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'The record is already deleted';
    }

}