<?php

use yii\db\Migration;

/**
 * Class m190213_230234_test
 */
class m190213_230234_test extends Migration
{
    public function up()
    {
        $this->createTable('test', [
            'id' => $this->primaryKey(),
            'deleted' => $this->timestamp()->null()->defaultValue(null),
        ]);
    }

    public function down()
    {
        $this->dropTable('test');
    }
}
