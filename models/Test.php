<?php

namespace app\models;

use app\components\_UndeletableActiveRecord;

/**
 * тестовая модель
 * @property int $id
 * @property string $deleted
 * @property bool $isDeleted
 */
class Test extends _UndeletableActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public static function deletedField(): string
    {
        return 'deleted';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('test', 'ID'),
            'deleted' => \Yii::t('test', 'Дата-время удаления'),
            'isDeleted' => \Yii::t('test', 'Признак удаления строки'),
        ];
    }
}
