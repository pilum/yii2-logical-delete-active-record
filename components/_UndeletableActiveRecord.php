<?php

namespace app\components;

use app\exceptions\RecordAlreadyDeletedException;
use app\exceptions\RecordNotDeletedException;
use yii\base\InvalidConfigException;
use yii\db\Expression;

/**
 * Класс-модель для таблиц БД строки в которых не удаляются физически, а только помечаются удаленными
 * Сделан абстрактным, потому, что требуется в явном виде задать имя поля, указывающего на то, что запись удалена
 * @property bool isDeleted
 */
abstract class _UndeletableActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * Возвращает имя поля, указывающего на то, что запись удалена
     * @static @abstract
     * @return string
     */
    public static abstract function deletedField() : string;

    /**
     * Простейшая предосторожность
     * @throws InvalidConfigException - в случае, когда получаем из deletedField() пустое имя поля
     */
    public function __construct($config = [])
    {
        if(!static::deletedField()){
            throw new InvalidConfigException("deletedField method can't return empty string.");
        }
        parent::__construct($config);
    }

    /**
    * Переопределим удаление на "нижнем" уровне
    * @param string|array $condition the conditions that will be put in the WHERE part of the DELETE SQL.
    * Please refer to [[Query::where()]] on how to specify this parameter.
    * @param array $params the parameters (name => value) to be bound to the query.
    * @return int the number of rows deleted
    * @throws \Exception
    */
    public static function deleteAll($condition = null, $params = [])
    {
        $command = static::getDb()->createCommand();
        $command->update(static::tableName(), [ static::deletedField() => (new Expression('NOW()')) ], $condition, $params);

        return $command->execute();
    }

    /**
     * теперь переопределим метод более высокого уровня для запрета повторного удаления
     * @return int|false
     * @throws RecordAlreadyDeletedException | \Exception | \Throwable
     */
    public function delete()
    {
        if ($this->isDeleted){
            throw new RecordAlreadyDeletedException();
        }

        return parent::delete();
    }

    /**
     * восстановление удаленных записей
     * @return int|false
     * @throws RecordNotDeletedException | \Exception | \Throwable
     */
    public function recover()
    {
        if (!$this->isDeleted){
            throw new RecordNotDeletedException();
        }

        return parent::updateAttributes([static::deletedField() => null]);
    }

    /**
     * проверка на признак удаленности
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->{static::deletedField()} !== null;
    }
}
