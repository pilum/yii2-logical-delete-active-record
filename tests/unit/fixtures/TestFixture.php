<?php

namespace tests\unit\fixtures;

use yii\test\ActiveFixture;

/**
 * Фикстура для таблицы test
 * @package tests\unit\fixtures
 */
class TestFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Test';
}
