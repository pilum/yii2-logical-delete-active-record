<?php

namespace tests\models;

use app\exceptions\RecordNotDeletedException;
use app\models\Test;
use Codeception\Specify;
use Codeception\Exception\ModuleException;
use tests\unit\fixtures\TestFixture;
use app\exceptions\RecordAlreadyDeletedException;

/**
 * Тестирование работы с моделью Test
 * @package tests\models
 */
class TestTest extends \Codeception\Test\Unit
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Загружаемые фикстуры
     * @return array
     */
    public function _fixtures() {
        return [
            'test' => TestFixture::class,
        ];
    }

    /**
     * Тестирование удаления
     */
    public function testDelete()
    {
        /** @var Test $row */
        $this->specify("Мы можем пометить строку строку удаленной, но физически она сохранится в БД", function () {
            $row = Test::findOne(1);
            verify($row->delete())->equals(1);
            $this->tester->seeRecord(Test::class, ['id' => $row->id]);
            $row->refresh();
            verify($row->isDeleted)->true();
        });

        $this->specify("Попытка удаления уже удаленной строки выдаст ошибку", function () {
            $this->tester->expectThrowable(RecordAlreadyDeletedException::class, function() {
                $row = Test::findOne(2);
                $row->delete();
            });
        });
    }

    /**
     * Тестирование восстановления удаленной записи
     */
    public function testRecover()
    {
        /** @var Test $row */
        $this->specify("Мы можем восстановить удаленную строку", function () {
            $row = Test::findOne(2);
            verify($row->recover())->equals(1);
            $row->refresh();
            verify($row->isDeleted)->false();
        });

        $this->specify("Попытка восстановления НЕ удаленной строки выдаст ошибку", function () {
            $this->tester->expectThrowable(RecordNotDeletedException::class, function() {
                $row = Test::findOne(1);
                $row->recover();
            });
        });
    }
}
